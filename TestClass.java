import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
public class TestClass {
	int newPlayer, previousPlayer;
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	public void process() throws Exception{
		String[] s = (br.readLine()).split(" ");
		if(s[0].equalsIgnoreCase("p")){
			int x = Integer.parseInt(s[1]);
			throwAnother(x);
		}
		if(s[0].equalsIgnoreCase("b")){
			passBack();
		}
	}
	public void throwAnother(int playerID){
		previousPlayer = newPlayer;
		newPlayer = playerID;
	}
	
	public void passBack(){
		newPlayer = newPlayer + previousPlayer;
		previousPlayer = newPlayer - previousPlayer;
		newPlayer = newPlayer - previousPlayer;
	}
	
	public static void main(String[] args) throws java.lang.Exception {
		int N,ID;
		TestClass fh = new TestClass();
		int T = Integer.parseInt(fh.br.readLine());
		int output[] = new int[T];
		for(int a =0;a<T;a++){
			String str[] = (fh.br.readLine()).split(" ");
			N = Integer.parseInt(str[0]);
			ID = Integer.parseInt(str[1]);
			fh.newPlayer = ID;
			for(int b=0;b<N;b++){
				fh.process();
			}
			output[a] = fh.newPlayer;
			fh.newPlayer =0;
			fh.previousPlayer =0;
		}
		for(int a=0;a<T;a++){
			System.out.println("Player "+output[a]);
		}
		
	}
}